let selected_vehicle = 0;
let fleetInfo = [];
window.addEventListener("load", function(e){  
  fetch(`https://porter.ae/website/get_vehicles_fare_details?geo_region_id=1`)
        .then((response) => response.json())
        .then((data) => data).then((fleetData) =>{
        fleetInfo = fleetData.response;
        // console.log( fleetData.response)
      
        let fleetDiv = document.getElementsByClassName("tariff-fleet")[0];
        selected_vehicle=fleetData.response[0].id;
        
        fleetData.response.forEach((data) => {
        const vehicleBox = document.createElement("div");
        vehicleBox.className = "vehicle-box";
        vehicleBox.id=data.id;
        const vehicleItem = document.createElement("div");
        vehicleItem.className = "vehicle-item";
        const imgWrapper = document.createElement("div");
        imgWrapper.className = "img-wrapper";
        const background = document.createElement("div");
        background.className = "background ";
        background.style=`background-image: url(${data.vehicle_icon_url});`;
        background.vehicleID=data.id;
        background.addEventListener('click', myFunc, false);
          
        if(selected_vehicle == data.id){
          vehicleBox.className += " selected"
        }
          
        const title = document.createElement("div");
        title.className = "title";
        title.innerText= data.display_name;
        const vehicleSelector = document.createElement("div");
        vehicleSelector.className = "vehicle-selector";
          
        imgWrapper.appendChild(background)
        imgWrapper.appendChild(title)
        vehicleItem.appendChild(imgWrapper);
        vehicleBox.appendChild(vehicleItem);
        vehicleBox.appendChild(vehicleSelector);
        fleetDiv.appendChild(vehicleBox);
        });

    myFunc3(selected_vehicle);

  })
  });

function myFunc(e) {
  let id = e.currentTarget.vehicleID;
  document.getElementById(selected_vehicle).className="vehicle-box";
  document.getElementById(id).className = "vehicle-box selected";
  selected_vehicle = id;
  
  let vehicleInfo = fleetInfo.find((vehicle) => vehicle.id == selected_vehicle);
  
  document.getElementsByClassName("vehicle-detail-name")[0].innerText= "RENT " + vehicleInfo.display_name.toUpperCase();
  document.getElementsByClassName("detail-capacity-weight")[0].innerText = vehicleInfo.capacity_display;
  document.getElementsByClassName("detail-dimensions")[0].innerText = vehicleInfo.size_display;
  document.getElementsByClassName("base-fare-price")[0].innerText = vehicleInfo.base_fare_display;
  document.getElementsByClassName("condition-text")[0].innerText = vehicleInfo.free_time_distance_text;
  document.getElementsByClassName("vehicle-img")[0].src = vehicleInfo.image_url;
  document.getElementsByClassName("vehicle-img")[0].alt = vehicleInfo.image_alt_text;
  if(vehicleInfo.url != null){
    let knowMoreBtn = document.createElement('a');
    knowMoreBtn.className="link bold-txt-btn know-more-btn";
    knowMoreBtn.href="https://porter.in/" + vehicleInfo.url;
    knowMoreBtn.innerText="KNOW MORE >"
    document.getElementsByClassName("tariff-know-more")[0].innerHTML = "";
    document.getElementsByClassName("tariff-know-more")[0].appendChild(knowMoreBtn);
  }
  else{
    document.getElementsByClassName("tariff-know-more")[0].innerHTML = "";
  }
  
}

function myFunc3(e){

  let id = e;
  // console.log(id);
  document.getElementById(selected_vehicle).className="vehicle-box";
  document.getElementById(id).className = "vehicle-box selected";
  selected_vehicle = id;
  
  let vehicleInfo = fleetInfo.find((vehicle) => vehicle.id == selected_vehicle);
  
  document.getElementsByClassName("vehicle-detail-name")[0].innerText= "RENT " + vehicleInfo.display_name.toUpperCase();
  document.getElementsByClassName("detail-capacity-weight")[0].innerText = vehicleInfo.capacity_display;
  document.getElementsByClassName("detail-dimensions")[0].innerText = vehicleInfo.size_display;
  document.getElementsByClassName("base-fare-price")[0].innerText = vehicleInfo.base_fare_display;
  document.getElementsByClassName("condition-text")[0].innerText = vehicleInfo.free_time_distance_text;
  document.getElementsByClassName("vehicle-img")[0].src = vehicleInfo.image_url;
  document.getElementsByClassName("vehicle-img")[0].alt = vehicleInfo.image_alt_text;
  if(vehicleInfo.url != null){
    let knowMoreBtn = document.createElement('a');
    knowMoreBtn.className="link bold-txt-btn know-more-btn";
    knowMoreBtn.href="https://porter.ae/" + vehicleInfo.url;
    knowMoreBtn.innerText="KNOW MORE";
    // knowMoreBtn.innerHTML=" <svg class='MuiSvgIcon-root MuiSvgIcon-fontSizeLarge' focusable='false' viewBox='0 0 24 24' aria-hidden='true'><path d='M9.31 6.71c-.39.39-.39 1.02 0 1.41L13.19 12l-3.88 3.88c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41L10.72 6.7c-.38-.38-1.02-.38-1.41.01z'></path></svg>"
    document.getElementsByClassName("tariff-know-more")[0].innerHTML = "";
    document.getElementsByClassName("tariff-know-more")[0].appendChild(knowMoreBtn);
  }
  else{
    document.getElementsByClassName("tariff-know-more")[0].innerHTML = "";
  }

}

